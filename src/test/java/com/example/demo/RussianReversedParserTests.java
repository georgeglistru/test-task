package com.example.demo;

import com.example.demo.domain.Word;
import com.example.demo.services.TextParser;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class RussianReversedParserTests {

	@Test
	void contextLoads() {
		assertTrue(StringUtils.isBlank(""));
	}

	@Autowired
	private TextParser russianReversedTextParser;

	@Test
	void testParseSimleString() {
		List<Word> words = russianReversedTextParser.parseInput("акортс яатсорп отэ");
		System.out.println(words);
		assertEquals(Arrays.asList(new Word("отэ"),
				new Word("яатсорп"),
				new Word("акортс"))
				, words);

	}


}
