package com.example.demo;

import com.example.demo.domain.SearchResult;
import com.example.demo.domain.Word;
import com.example.demo.services.SearchService;
import com.example.demo.services.TextParser;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class SearchTests {

	@Test
	void contextLoads() {
		String str = "reversed string with spaces";
		int lastIndexOf = str.lastIndexOf(" ", 15);
		System.out.println(str.length());
		System.out.println(str.charAt(lastIndexOf-1));
	}

	@Autowired
	private TextParser russianTextParser;

	@Autowired
	private SearchService searchService;

	@Test
	void testParseSimleString() {
		String inputText = "красной лакее";
		String searchText = "В тексте встречается красный лакей. Так говорила в июле 1805 года известная Анна Павловна Шерер, фрейлина и приближенная императрицы Марии Феодоровны, встречая важного и чиновного князя Василия, первого приехавшего на ее вечер. Анна Павловна кашляла несколько дней, у нее был грипп, как она говорила (грипп был тогда новое слово, употреблявшееся только редкими). В записочках, разосланных утром с красным лакеем, было написано без различия во всех:";
//		String searchText = "встречается красная лакея в тексте";

		List<Word> inputWords = russianTextParser.parseInput(inputText);
		List<Word> searchTextWords = russianTextParser.parseInput(searchText);
		System.out.println(inputWords);
		System.out.println(searchTextWords);

		List<SearchResult> match = searchService.findMatch(inputWords, searchTextWords);
		System.out.println(match.size());
		System.out.println(match);

	}

}
