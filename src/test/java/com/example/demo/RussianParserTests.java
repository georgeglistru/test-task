package com.example.demo;

import com.example.demo.domain.Word;
import com.example.demo.services.TextParser;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class RussianParserTests {

	@Test
	void contextLoads() {
		assertTrue(StringUtils.isBlank(""));
	}

	@Autowired
	private TextParser russianTextParser;

	@Test
	void testParseSimleString() {
		List<Word> words = russianTextParser.parseInput("это простая строка");
		System.out.println(words);
		assertEquals(Arrays.asList(new Word("это"),
				new Word("простая"),
				new Word("строка"))
				, words);

	}

	@Test
	void testParseWithComma() {
		List<Word> words = russianTextParser.parseInput("это строка для поиска, с запятой");
		System.out.println(words);
		assertEquals(Arrays.asList(new Word("это"),
				new Word("строка"),
				new Word("для"),
				new Word("поиска"),
				new Word("с"),
				new Word("запятой"))
				, words);

	}

	@Test
	void testParseWithLineBreak() {
		List<Word> words = russianTextParser.parseInput("это строка для поиска, с запят-ой");
		System.out.println(words);
		assertEquals(Arrays.asList(new Word("это"),
				new Word("строка"),
				new Word("для"),
				new Word("поиска"),
				new Word("с"),
				new Word("запятой"))
				, words);

	}

	@Test
	void testParseWithLineBreak2() {
		List<Word> words = russianTextParser.parseInput("это строка для поис-ка, с запят-ой");
		System.out.println(words);
		assertEquals(Arrays.asList(new Word("это"),
				new Word("строка"),
				new Word("для"),
				new Word("поиска"),
				new Word("с"),
				new Word("запятой"))
				, words);

	}

	@Test
	void testWarAndPeace() {
		List<Word> words = russianTextParser.parseInput("красной  лакея");
		System.out.println(words);
		assertEquals(Arrays.asList(new Word("красной"),
				new Word("лакея"))
				, words);

	}

}
