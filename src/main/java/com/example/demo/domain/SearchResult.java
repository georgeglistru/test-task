package com.example.demo.domain;

import java.util.List;
import java.util.Objects;

public class SearchResult {
    private int startPosition;
    private int endPosition;

    private List<Word> matchedWords;

    public SearchResult() {
    }

    public SearchResult(int startPosition, int endPosition, List<Word> matchedWords) {
        this.startPosition = startPosition;
        this.endPosition = endPosition;
        this.matchedWords = matchedWords;
    }

    public int getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }

    public int getEndPosition() {
        return endPosition;
    }

    public void setEndPosition(int endPosition) {
        this.endPosition = endPosition;
    }

    public List<Word> getMatchedWords() {
        return matchedWords;
    }

    public void setMatchedWords(List<Word> matchedWords) {
        this.matchedWords = matchedWords;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchResult that = (SearchResult) o;
        return startPosition == that.startPosition &&
                endPosition == that.endPosition &&
                Objects.equals(matchedWords, that.matchedWords);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startPosition, endPosition, matchedWords);
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "startPosition=" + startPosition +
                ", endPosition=" + endPosition +
                ", matchedWords=" + matchedWords +
                '}';
    }
}
