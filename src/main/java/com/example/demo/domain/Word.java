package com.example.demo.domain;

import java.util.List;
import java.util.Objects;

public class Word {

    private String originalContent;
    private String dictionaryForm;
    private List<String> otherDictionaryForms;

    public Word(String originalContent) {
        this.originalContent = originalContent;
    }

    public String getOriginalContent() {
        return originalContent;
    }

    public void setOriginalContent(String originalContent) {
        this.originalContent = originalContent;
    }

    public String getDictionaryForm() {
        return dictionaryForm;
    }

    public void setDictionaryForm(String dictionaryForm) {
        this.dictionaryForm = dictionaryForm;
    }

    public List<String> getOtherDictionaryForms() {
        return otherDictionaryForms;
    }

    public void setOtherDictionaryForms(List<String> otherDictionaryForms) {
        this.otherDictionaryForms = otherDictionaryForms;
    }

    @Override
    public String toString() {
        return "Word{" +
                "originalContent='" + originalContent + '\'' +
                ", dictionaryForm='" + dictionaryForm + '\'' +
                ", otherDictionaryForms=" + otherDictionaryForms +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word = (Word) o;
        return originalContent.equals(word.originalContent);
    }

    public boolean approximateMatch(Word anotherWord) {
        if (this.getOtherDictionaryForms().contains(anotherWord.dictionaryForm) || anotherWord.getOtherDictionaryForms().contains(this.dictionaryForm)) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(originalContent);
    }
}
