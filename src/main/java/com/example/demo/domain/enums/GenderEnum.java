package com.example.demo.domain.enums;

public enum GenderEnum {
    MALE, FEMALE, NEUTRAL
}
