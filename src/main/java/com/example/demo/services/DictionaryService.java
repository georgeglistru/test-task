package com.example.demo.services;

import java.util.List;

public interface DictionaryService {
    String findDictionaryForm(String word);

    List<String> findSynonyms(String dictionaryForm);
}
