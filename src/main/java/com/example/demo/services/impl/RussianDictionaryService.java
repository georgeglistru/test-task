package com.example.demo.services.impl;

import com.example.demo.services.DictionaryService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class RussianDictionaryService implements DictionaryService {
    @Override
    public String findDictionaryForm(String word) {
//        if (word.equalsIgnoreCase("война")
//        || word.equalsIgnoreCase("войны")
//                || word.equalsIgnoreCase("войну")
//                || word.equalsIgnoreCase("войне")
//        ) {
//            return "война";
//        }

        if (word.equalsIgnoreCase("лакей")
                || word.equalsIgnoreCase("лакеи")
                || word.equalsIgnoreCase("лакея")
                || word.equalsIgnoreCase("лакеем")
                || word.equalsIgnoreCase("лакеями")
        ) {
            return "лакей";
        }

        if (word.equalsIgnoreCase("красный")
                || word.equalsIgnoreCase("красная")
                || word.equalsIgnoreCase("красным")
                || word.equalsIgnoreCase("красном")
                || word.equalsIgnoreCase("красной")
        ) {
            return "красный";
        }

//        if (word.equalsIgnoreCase("мир")
//                || word.equalsIgnoreCase("мира")
//                || word.equalsIgnoreCase("миры")
//                || word.equalsIgnoreCase("мире")
//        ) {
//            return "мир";
//        }

        return word;
    }

    @Override
    public List<String> findSynonyms(String dictionaryForm) {
        if (dictionaryForm.equalsIgnoreCase("лакей")) {
            return Arrays.asList("лакеи", "лакеем", "лакее", "лакей", "лакей");
        }
        if (dictionaryForm.equalsIgnoreCase("красный")) {
            return Arrays.asList("красная", "красным", "красном", "красный");
        }
        return new ArrayList<>();
    }
}
