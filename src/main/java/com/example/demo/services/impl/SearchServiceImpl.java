package com.example.demo.services.impl;

import com.example.demo.domain.SearchResult;
import com.example.demo.domain.Word;
import com.example.demo.services.SearchService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SearchServiceImpl implements SearchService {
    @Override
    public List<SearchResult> findMatch(List<Word> inputWords, List<Word> textWords) {
        List<SearchResult> result = new ArrayList<>();
        outer:
        for (int i = 0; i < textWords.size() - inputWords.size(); i++) {
            for (int j = 0; j < inputWords.size(); j++) {
                if (!textWords.get(i+j).approximateMatch(inputWords.get(j))) {
                    continue outer;
                }
            }
            result.add(new SearchResult(i, i+ inputWords.size(), textWords.subList(i, i + inputWords.size())));
        }
        return result;
    }
}
