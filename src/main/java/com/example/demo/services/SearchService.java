package com.example.demo.services;

import com.example.demo.domain.SearchResult;
import com.example.demo.domain.Word;

import java.util.List;

public interface SearchService {
    List<SearchResult> findMatch(List<Word> inputWords, List<Word> textWords);
}
