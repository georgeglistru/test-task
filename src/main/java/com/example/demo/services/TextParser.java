package com.example.demo.services;

import com.example.demo.domain.Word;

import java.util.List;

public interface TextParser {
    List<Word> parseInput(String input);

    List<String> getWordSeparators();

    List<String> getLineBreakSeparators();

    void addNewWord(List<Word> words, String newWord);

    DictionaryService getDictionaryService();


}
