package com.example.demo.services.parsers;

import com.example.demo.domain.Word;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class LeftToRightParser extends AbstractTextParser {
    @Override
    public List<Word> parseInput(String input) {
        int i = 0;
        List<Word> result = new ArrayList<>();
        final int INPUT_LENGHT = input.length();
        while(i < INPUT_LENGHT) {
            List<Integer> nextSeparatorPositions = new ArrayList<>();
            for (String wordSeparator : getWordSeparators()) {
                int separatorPosition = input.indexOf(wordSeparator, i);
                if (separatorPosition != -1) {
                    nextSeparatorPositions.add(separatorPosition);
                }
            }
            if (nextSeparatorPositions.size()==0) {
                addNewWord(result, input.substring(i));
                break;
            }
            Integer nextSeparator = Collections.min(nextSeparatorPositions);
            addNewWord(result, input.substring(i, nextSeparator));
            i = nextSeparator + 1;
        }
        return result;
    }
}
