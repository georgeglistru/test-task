package com.example.demo.services.parsers;

import com.example.demo.domain.Word;
import com.example.demo.services.DictionaryService;
import com.example.demo.services.TextParser;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public abstract class AbstractTextParser implements TextParser {
    @Override
    public void addNewWord(List<Word> words, String originalNewWordContent) {

        for (String lineBreakSeparator : getLineBreakSeparators()) {
            if (originalNewWordContent.contains(lineBreakSeparator))  {
                originalNewWordContent = originalNewWordContent.replaceAll(lineBreakSeparator, "");
            }
        }

        if (StringUtils.isBlank(originalNewWordContent)) {
            return;
        }

        DictionaryService dictionaryService = getDictionaryService();
        Word word = new Word(originalNewWordContent);
        String dictionaryForm = dictionaryService.findDictionaryForm(originalNewWordContent);
        word.setDictionaryForm(dictionaryForm);
        word.setOtherDictionaryForms(dictionaryService.findSynonyms(dictionaryForm));
        words.add(word);
    }
}
