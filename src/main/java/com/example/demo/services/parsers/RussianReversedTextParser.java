package com.example.demo.services.parsers;

import com.example.demo.services.DictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class RussianReversedTextParser extends RightToLeftParser {

    @Autowired
    private DictionaryService russianDictionaryService;

    @Override
    public List<String> getWordSeparators() {
        return Arrays.asList(" ");
    }

    @Override
    public List<String> getLineBreakSeparators() {
        return Arrays.asList("-");
    }

    @Override
    public DictionaryService getDictionaryService() {
        return russianDictionaryService;
    }
}
